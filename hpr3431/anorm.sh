#!/bin/bash
#####################
# anorm by BlacKernel
#####################
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.
#####################
# This function will take input from an audio file or stdin
# and process it to normalize the audio with ffmpeg loudnorm and ffmpeg-lh
# and output to stdout or another audio file

# NOTE! This script *requires* ffmpeg-lh: https://github.com/indiscipline/ffmpeg-loudnorm-helper/

# If there is a first argument (input)...
if [ $1 ]; then
	in="$1"	#... use it.
else
	in="-" #... otherwise, use stdin.
fi

# If there is a second argument (output)...
if [ $2 ]; then
	out="$2" #... use it.
else
	out="-f nut -" #... otherwise, use stdout.
fi

# If there is more than two arguments...
if [[ $# > 2 ]]; then
	echo "anorm: requires 2 or fewer arguments"	#... tell the user...
	exit 1						#... and panic.
fi

# If there is an output file, then we will print status messages.
# Otherwise, we won't, so it doesn't mangle the data
if [ $2 ]; then echo "atrim: silencing $in and saving to $out..."; fi
ffmpeg -i $in $(ffmpeg-lh "$in") $out \
	2>/dev/null \
	#1>/dev/null
if [ $2 ]; then echo "anorm: done"; fi

# Clean up
unset in
unset out
