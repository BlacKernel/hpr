#!/bin/bash
#####################
# top-tail by BlacKernel
#####################
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.
#####################
# This function will take input from an audio file or stdin
# and process it to add a given top and tail audio files to
# the beginning and end of the input audio
# and it will output to stdout or another audio file

top=""		# TODO: Set this to the audio you want pre-pended to your audio
tail=""		# TODO: Set this to the audio you want appended   to your audio

# If there is a first argument (input)...
if [ $1 ]; then
	in="$1"	#... use it.
else
	in="-" #... otherwise, use stdin.
fi

# If there is a second argument (output)...
if [ $2 ]; then
	out="$2" #... use it.
else
	out="-f nut -" #... otherwise, use stdout.
fi

# If there is more than two arguments...
if [[ $# > 2 ]]; then
	echo "top-tail: requires 2 or fewer arguments"	#... tell the user...
	exit 1						#... and panic.
fi

# If there is an output file, then we will print status messages.
# Otherwise, we won't, so it doesn't mangle the data
if [ $2 ]; then echo "atrim: silencing $in and saving to $out..."; fi
ffmpeg -i "$top" -i $in "$tail" -vn -filter_complex "
[0][1]acrossfade=d=1:c1=tri:c2=tri[a01];
[a01][2]acrossfade=d=1:c1=tri:c2=tri" \
					2>/dev/null \
					#1>/dev/null
if [ $2 ]; then echo "atrim: done"; fi

# Clean up
unset in
unset out
unset top
unset tail
